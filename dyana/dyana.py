#!/usr/bin/env python3
# coding: utf-8

"""DirectorY ANAlyser

LICENSE
   MIT (https://mit-license.org/)

COPYRIGHT
   © 2020 Steffen Brinkmann <s-b@mailbox.org>
"""

__version__ = '0.1.0'

import argparse
import configparser
import logging
from math import ceil
import os
from pathlib import Path
import sys
from typing import List, Optional, Union

_blocks = (' ', '░', '▒', '▓', '█')

def _du(dir):
    return ''

def _add_bars(inp:str, data:list, vpos:Union[str, int] = 'top', hpos:int = 1) -> str:
    """ add bars to the right of each line"""
    # set vpos if necessary
    if vpos == 'top':
        vpos = 0
    elif vpos == 'bottom':
        vpos = len(inp) - len(data)

    # split input string into lines
    inp = inp.splitlines()

    # get longest line
    max_length = 0
    for line in inp:
        max_length = max([max_length, len(line)])

    # append the bars to each line
    result = []
    for i, line in enumerate(inp):
        if i < vpos:
            result.append(line)
            continue
        width_total = os.get_terminal_size().columns - max_length - hpos
        width_bar = ceil(width_total * data[i - vpos])
        result.append(line
                      + ' ' * (hpos + max_length - len(line))
                      + _blocks[4] * width_bar
                      + _blocks[1] * (width_total - width_bar))

    return '\n'.join(result)

def _df(dirs:List[str] = []):
    result = ''
    if len(dirs) != 0:
        dev_ids = [os.stat(d).st_dev for d in dirs]
    mtab = open(Path('/etc/mtab')).readlines()
    partitions = []
    for i, line in enumerate(mtab):
        if len(dirs) != 0:
            try:
                dev = os.stat(line.split()[1]).st_dev
            except PermissionError:
                continue

            if dev in dev_ids:
                partitions.append(line.strip().split())
        else:
            partitions.append(line.strip().split())

    fmt = '{:12} {:>12} {:>10} {:>9} {:>4}% {:>}'
    result_str = []
    result_use = []
    for line in partitions:
        if len(dirs) != 0 or line[0] in ['dev', 'run', 'tmpfs'] or line[0][0] == '/':
            try:
                use = ((os.statvfs(line[1]).f_blocks - os.statvfs(line[1]).f_bfree)
                       / (os.statvfs(line[1]).f_bavail + os.statvfs(line[1]).f_blocks
                          - os.statvfs(line[1]).f_bfree))
                use_perc = ceil(use * 100)
            except ZeroDivisionError:
                use = 0
                use_perc = '-'

            result_str.append(fmt.format(line[0],
                                         int(os.statvfs(line[1]).f_blocks * os.statvfs(line[1]).f_frsize / 1024),
                                         int((os.statvfs(line[1]).f_blocks - os.statvfs(line[1]).f_bfree)
                                             * os.statvfs(line[1]).f_frsize / 1024),
                                         int(os.statvfs(line[1]).f_bavail * os.statvfs(line[1]).f_frsize / 1024),
                                         use_perc,
                                         line[1]))
            result_use.append(use)

    result_str = '\n'.join(result_str)
    result_str = fmt.format('Filesystem',
                            '1K-blocks',
                            'Used',
                            'Available',
                            'Use',
                            'Mounted on') + '\n' + result_str
    return result_str, result_use

def _set_logging_level(quiet: bool, verbose: bool, debug: bool) -> None:  # pragma: no cover
    if debug is True:
        logging.getLogger().setLevel(logging.DEBUG)
        logging.info('debug mode engaged')
    if verbose is True:
        logging.getLogger().setLevel(logging.INFO)
    if quiet is True:
        logging.getLogger().setLevel(logging.ERROR)


def _parse_args(argv=None):
    parser = argparse.ArgumentParser(description='DirectorY ANAlyser. '
                                     'Please have a look at the documentation (https://pypi.org/project/dyana/) '
                                     'for further information on how tho use this software.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('dir', type=str, nargs='*',
                        help='directory or directories to be analyzed')
    parser.add_argument('-c', '--config', type=str,
                        help='configuration file. If not given, ~/.config/dyana/config is used. '
                        'Settings in the config file are overwritten by command line options.')
    parser.add_argument('-l', '--logfile', type=str,
                        help='log file. If not given, ~/.config/dyana/log is used')
    parser.add_argument('-i', '--interactive', action='store_true',
                        help='interactive mode. This is the default unless the configuration file says '
                        'otherwise. In this case, this parameter overwrites the config file.')
    parser.add_argument('-o', '--output-file', type=str, metavar='FILE',
                        help='the output file. If it is set the interactive features are disabled and '
                        'the output is written to a file. Use -o- to write to stdout')
    parser.add_argument('-q', '--quiet', action='store_true',
                        help='switch off logging output except for error messages. This will overwrite -v.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='more verbose logging output')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='switch on debug mode. Even more verbose logging output')
    parser.add_argument('--version', action='version', version=f'%(prog)s {__version__}',
                        help='show the version of this software')
    return parser.parse_args(argv)


def run(argv=None):
    """the command line tool. Please use the ``--help`` option to get help."""

    # parse the command line options
    args = _parse_args(argv)

    # print the logo and version
    if args.quiet is False:  # pragma: no cover
        print(f'dyana v{__version__}')

    # make sure dyana's data directory is in place
    Path(os.path.expanduser('~/.config/dyana')).mkdir(parents=True, exist_ok=True)

    # set debug mode
    _DYANA_DEBUG_MODE = args.debug

    # initialize logging settings
    logfile = args.logfile or Path(os.path.expanduser('~/.config/dyana/log'))
    logging_format = 'dyana | %(asctime)s | %(levelname)-8s | %(message)s'
    logging.basicConfig(filename=logfile, format=logging_format, level=logging.WARNING)

    # set verbosity level
    _set_logging_level(args.quiet, args.verbose, args.debug)
    logging.debug(args)

    # make sure default configuration file is in place
    configfile = Path(os.path.expanduser('~/.config/dyana/config'))
    if not configfile.exists():
        configfile.write_text(Path('default.cfg').read_text())

    # read configuration
    # TODO: Do we need a config file?
    configfile = args.config or configfile
    config = configparser.ConfigParser()
    config.read_file(open(configfile))


    # set output file
    if args.interactive is True:
        output_file = None
    elif args.output_file == '-':
        output_file = sys.stdout
    elif args.output_file is not None and args.output_file != '':
        output_file = args.output_file
    elif config['general']['output_file'] == 'interactive':
        output_file = None
    elif config['general']['output_file'] == 'stdout':
        output_file = sys.stdout
    else:
        output_file = open(config['general']['output_file'], 'w')

    logging.debug(f'output_file: {output_file}')

    if output_file is None:
        raise NotImplementedError('Interactive mode is not implemented yet.')

    result_du = _du(args.dir)

    result_df, result_use = _df(args.dir)
    result_df = _add_bars(result_df, result_use, vpos=1, hpos=2)


    output_file.write(result_du)
    output_file.write('\n')
    output_file.write(result_df)
    output_file.write('\n')


if __name__ == '__main__':
    run()
